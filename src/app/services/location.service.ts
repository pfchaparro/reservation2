import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private url_base: string = 'https://apex.oracle.com/pls/apex/pfchaparro1/v1/';;
  private url_ave: string = 'location';

  constructor(private http: HttpClient) { }
  
  public listLocation(): Observable<any>{
    return this.http.get(this.url_base + this.url_ave);
  }
  
}
