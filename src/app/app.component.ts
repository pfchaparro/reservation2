import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public myLogin: FormGroup;
  public dataUser: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private serviceUser: UserService, 
    public formBuilder: FormBuilder, 
    private router: Router
  ) {
    this.initializeApp();
    this.myLogin = this.createMyForm();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {

  }

  private createMyForm() {
    return this.formBuilder.group({
      correo: ['', Validators.required],
      contrasena: ['', Validators.required]
    });
  }

  public login() {
    console.log('hola');
    this.router.navigate(['home']);
   // this.router.navigateByUrl('home');
    /*
    console.log(this.myForm.value);
    this.serviceUser.login(this.myForm.value.correo, this.myForm.value.contrasena).subscribe(
      data => {
        this.dataUser = data.items;
        if (this.dataUser > 0) {
          this.router.navigate(['inicio']);
          alert('Bienvenido!');
          console.log(this.dataUser);
        } else {
          alert('Credenciales incorrectas!');
        }
      },
      err => {
        console.log(err);
        alert('Error inesperado al loguaerse');
      }
    );
    */
  }
}
